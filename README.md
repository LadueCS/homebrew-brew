# LadueCS Brew

## How do I install these formulae?

`brew install laduecs/brew/<formula>`

Or `brew tap laduecs/brew` and then `brew install <formula>`.

## Documentation

`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).

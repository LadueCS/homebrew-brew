class GopencontestCli < Formula
  desc 'Command-line OpenContest client written in Go'
  homepage 'https://laduecs.github.io'
  url 'https://github.com/LadueCS/GOpenContest-CLI/archive/refs/tags/2.2.tar.gz'
  sha256 '2eed6c15f8eef6a3f742d82360e600a2e96a0e50e2c639d6d0e23e7fc0e16eeb'
  license 'GPL-3.0-only'
  version '2.2'

  depends_on 'go' => :build

  def install
    system 'go', 'build'
    bin.install 'gocc'
  end

  test do
    system "#{bin}/gocc", 'about', '--server', 'https://contest.adawesome.tech'
  end
end
